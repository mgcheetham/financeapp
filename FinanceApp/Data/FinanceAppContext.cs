﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FinanceApp.Models;

namespace FinanceApp.Data
{
    public class FinanceAppContext : DbContext
    {
        /// <summary>
        /// New website context created from data models
        /// </summary>
        /// <param name="options"></param>
        public FinanceAppContext(DbContextOptions<FinanceAppContext> options) : base(options)
        {
        }

        public DbSet<Cryptocoins> Cryptocoins { get; set; }
        public DbSet<InvestmentData> InvestmentData { get; set; }
    }
}
