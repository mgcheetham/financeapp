﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinanceApp.Data;
using FinanceApp.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace FinanceApp.Controllers
{
    public class CryptocoinsController : Controller
    {
        private readonly FinanceAppContext _context;

        /// <summary>
        /// Assigns website context into controller on load
        /// </summary>
        /// <param name="context"></param>
        public CryptocoinsController(FinanceAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Updates the cryptocoins page on load with db data async
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.Cryptocoins.ToListAsync());
        }

        /// <summary>
        /// Specific get request to load current cryptocoins async
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Cryptocoins>> GetCryptocoins()
        {
            return await _context.Cryptocoins.ToArrayAsync();
        }

        /// <summary>
        /// Updates the cryptocoins db data from the coingecko api, returns new data to view
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Cryptocoins>> UpdateCryptocoins()
        {
            var coinList = new List<CoinData>();

            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.GetAsync("https://api.coingecko.com/api/v3/coins/markets?vs_currency=gbp&ids=ethereum%2Comisego%2Ceos%2Cripple%2Cstellar%2Cneo%2C0x%2Ckickico%2Cbitcoin%2Ctezos%2Ccelo-gold%2Cmaker%2Calgorand%2Cstellar&order=market_cap_desc&per_page=5&page=1&sparkline=false"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    coinList = JsonConvert.DeserializeObject<List<CoinData>>(apiResponse);
                }
            }

            if (ModelState.IsValid)
            {
                foreach (CoinData coins in coinList)
                {
                    //var cryptocoins = await _context.Cryptocoins.FindAsync(coins.id);
                    var coin = await _context.Cryptocoins.FirstOrDefaultAsync(x => x.coinId == coins.id);
                    
                    if (coin != null)
                    {
                        coin.currentValueCoin = (decimal)coins.current_price;
                        coin.currentTotalValue = coin.purchaseAmount * coin.currentValueCoin;
                    }
                }

                _context.SaveChanges();
            }

            return await _context.Cryptocoins.ToArrayAsync();
        }

        /// <summary>
        /// Adds a new cryptocoin to the table based on the form input data
        /// </summary>
        /// <param name="cryptocoins"></param>
        /// <returns></returns>
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IEnumerable<Cryptocoins>> AddCryptocoins([Bind("datePurchased,coinName,dateValueCoin,purchaseAmount")] Cryptocoins cryptocoins)
        {
            // To protect from overposting attacks, enable the specific properties you want to bind to, for 
            // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

            if (!string.IsNullOrEmpty(cryptocoins.coinName))
            {
                cryptocoins.coinId = cryptocoins.coinName.ToLower();
                cryptocoins.dateTotalValue = cryptocoins.dateValueCoin * cryptocoins.purchaseAmount;

                if (ModelState.IsValid)
                {
                    _context.Add(cryptocoins);
                    _context.SaveChanges();
                }
            }

            return await _context.Cryptocoins.ToArrayAsync();
        }

        /// <summary>
        /// Removes a cryptocoin based on the input from request
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveCryptocoins(string name)
        {
            var cryptocoins = await _context.Cryptocoins.FindAsync(name);
            
            _context.Cryptocoins.Remove(cryptocoins);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Edits the selected cryptocoin, updates db and return result to view
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cryptocoins"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCryptocoins(int id, [Bind("datePurchased,coinName,dateValueCoin,purchaseAmount,dateTotalValue,currentValueCoin,currentTotalValue")] Cryptocoins cryptocoins)
        {
            // To protect from overposting attacks, enable the specific properties you want to bind to, for 
            // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

            if (id != cryptocoins.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cryptocoins);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CryptocoinsExists(cryptocoins.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(cryptocoins);
        }

        /// <summary>
        /// Check cryptocoins exist before allowing edit action to proceed
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CryptocoinsExists(int id)
        {
            return _context.Cryptocoins.Any(e => e.ID == id);
        }
    }

    public class CoinData
    {
        public string id { get; set; }
        public string symbol { get; set; }
        public string name { get; set; }
        public float current_price { get; set; }
    }
}
