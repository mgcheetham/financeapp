﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceApp.Data;
using FinanceApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinanceApp.Controllers
{
    public class InvestmentDataController : Controller
    {
        private readonly FinanceAppContext _context;

        public InvestmentDataController(FinanceAppContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<InvestmentData>> GetChartData()
        {
            // Function required to get chart data from db
            var chartData = _context.InvestmentData.AsEnumerable()
                .OrderBy(x => x.currentDate);

            return await Task.FromResult(chartData);
        }

        [HttpGet]
        public async Task<InvestmentData> GetTotalsData()
        {
            var totalsData = _context.InvestmentData.AsEnumerable()
                                                    .OrderByDescending(x => x.currentDate)
                                                    .First();

            return await Task.FromResult(totalsData);
        }

        [HttpGet]
        public async Task<IEnumerable<InvestmentData>> AddNewDataPoint()
        {
            // Function required to add a new data point

            // Get and calculate current date

            // Load the current cryptocoins data from cryptocoins table

            // Foreach coin Retrieve the DateTotalValue
            // Calculate the original total investment value

            // Foreach coin retrieve the CurrentTotalValue
            // Calculate the current total investment value

            // Add all values as a new row to the chartdata table

            return await _context.InvestmentData.ToArrayAsync();
        }
    }
}
