﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FinanceApp.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cryptocoins",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    datePurchased = table.Column<DateTime>(type: "datetime2", nullable: false),
                    coinId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    coinName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dateValueCoin = table.Column<decimal>(type: "decimal(20,8)", nullable: false),
                    purchaseAmount = table.Column<decimal>(type: "decimal(20,8)", nullable: false),
                    dateTotalValue = table.Column<decimal>(type: "decimal(20,8)", nullable: false),
                    currentValueCoin = table.Column<decimal>(type: "decimal(20,8)", nullable: false),
                    currentTotalValue = table.Column<decimal>(type: "decimal(20,8)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cryptocoins", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "InvestmentData",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    currentDate = table.Column<DateTime>(type: "Date", nullable: false),
                    totalInvestment = table.Column<decimal>(type: "decimal(20,8)", nullable: false),
                    currentReturns = table.Column<decimal>(type: "decimal(10,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvestmentData", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cryptocoins");

            migrationBuilder.DropTable(
                name: "InvestmentData");
        }
    }
}
