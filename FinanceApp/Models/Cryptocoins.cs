﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceApp.Models
{
    public class Cryptocoins
    {
		[Key]
		public int ID { get; set; }

		public DateTime datePurchased { get; set; }

		public string coinId { get; set; }

		public string coinName { get; set; }

		[Column(TypeName = "decimal(20, 8)")]
		public decimal dateValueCoin { get; set; }

		[Column(TypeName = "decimal(20, 8)")]
		public decimal purchaseAmount { get; set; }

		[Column(TypeName = "decimal(20, 8)")]
		public decimal dateTotalValue { get; set; }

		[Column(TypeName = "decimal(20, 8)")]
		public decimal currentValueCoin { get; set; }

		[Column(TypeName = "decimal(20, 8)")]
		public decimal currentTotalValue { get; set; }
	}
}
