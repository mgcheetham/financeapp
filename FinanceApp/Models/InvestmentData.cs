﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceApp.Models
{
    public class InvestmentData
    {
        [Key]
        public int ID { get; set; }

        [Column(TypeName = "Date")]
        public DateTime currentDate { get; set; }

        [Column(TypeName = "decimal(20, 8)")]
        public decimal totalInvestment { get; set; }

        [Column(TypeName = "decimal(10, 2)")]
        public decimal currentReturns { get; set; }
    }
}
