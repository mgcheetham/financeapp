import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.css']
})

export class CryptoComponent implements OnInit {

  public cryptocoins: Cryptocoins[];
  public pageOfItems: Array<any>;
  public http: HttpClient;
  public baseUrl: string;
  public form: FormGroup;
  public formBuilder: FormBuilder;
  public activePage: number = 0;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    // Builds the component
    this.http = http;
    this.baseUrl = baseUrl;

    this.initialiseNewForm();
    this.loadCurrentCoins();
  };

  private initialiseNewForm() {

    // Creates a new form for new coins
    this.formBuilder = new FormBuilder();
    this.form = this.formBuilder.group(
      {
        datePurchased: 1/1/1900,
        coinName: '',
        dateValueCoin: 0,
        purchaseAmount: 0
      }
    );
  };

  onChangePage(pageOfItems: Array<any>) {

    // Updates current page view for table
    this.pageOfItems = pageOfItems;
  };

  loadCurrentCoins() {

    // http get request for the cryptocoins
    this.http.get<Cryptocoins[]>(this.baseUrl + 'Cryptocoins/GetCryptocoins').subscribe(result => {

      this.cryptocoins = result;

    }, error => console.error(error));
  };

  addNewCoin() {

    //console.log(this.form);

    // Builds a new form data of coins for the http post request
    var formData = new FormData();
    formData.append("datePurchased", this.form.get("datePurchased").value);
    formData.append("coinName", this.form.get("coinName").value);
    formData.append("dateValueCoin", this.form.get("dateValueCoin").value);
    formData.append("purchaseAmount", this.form.get("purchaseAmount").value);

    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');

    this.http.post<Cryptocoins[]>(this.baseUrl + 'Cryptocoins/AddCryptocoins', formData, { headers: headers }).subscribe(result => {

      this.cryptocoins = result;

    }, error => console.log(error));
  };

  removeCoin() {

    // Http get request to remove selected coin(s)
    this.http.get<Cryptocoins[]>(this.baseUrl + 'Cryptocoins/RemoveCryptocoins').subscribe(result => {

      this.cryptocoins = result;

    }, error => console.log(error));
  };

  updateCoinData() {

    // Updates coin values through http get request
    this.http.get<Cryptocoins[]>(this.baseUrl + 'Cryptocoins/UpdateCryptocoins').subscribe(result => {

      this.cryptocoins = result;

    }, error => console.log(error));
  };

  ngOnInit() {

  };
}

interface Cryptocoins {
  datePurchased: Date;
  coinName: string;
  dateValueCoin: number;
  purchaseAmount: number;
  dateTotalValue: number;
  currentValueCoin: number;
  currentTotalValue: number;
}
