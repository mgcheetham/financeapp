import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { ChartsModule, ThemeService } from 'ng2-charts';

import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CryptoComponent } from './crypto/crypto.component';
import { BudgetComponent } from './budget/budget.component';
import { MortgageComponent } from './mortgage/mortgage.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { InvestmentDataComponent } from './investment-data/investment-data.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CryptoComponent,
    BudgetComponent,
    MortgageComponent,
    JwPaginationComponent,
    LineChartComponent,
    InvestmentDataComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'budget', component: BudgetComponent },
      { path: 'mortgage', component: MortgageComponent },
      { path: 'crypto', component: CryptoComponent },
      { path: 'linecharts', component: LineChartComponent },
    ])
  ],
  exports: [

  ],
  providers: [ThemeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
