import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentDataComponent } from './investment-data.component';

describe('InvestmentDataComponent', () => {
  let component: InvestmentDataComponent;
  let fixture: ComponentFixture<InvestmentDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestmentDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
