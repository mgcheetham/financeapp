import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LineChartComponent } from '../line-chart/line-chart.component';

@Component({
  selector: 'app-investment-data',
  templateUrl: './investment-data.component.html',
  styleUrls: ['./investment-data.component.css']
})

export class InvestmentDataComponent implements OnInit {

  public investTotals: TotalsData;
  public http: HttpClient;
  public baseUrl: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.http = http;
    this.baseUrl = baseUrl;

    // http request to pull data
    this.http.get<TotalsData>(this.baseUrl + 'InvestmentData/GetTotalsData').subscribe(result => {

      this.investTotals = result;

      this.buildTotalsData();

    }, error => console.log(error));
  };

  private buildTotalsData() {

    // Helper to build the totals data before being displayed

    this.investTotals.currentDate = new Date(this.investTotals.currentDate);
    this.investTotals.percentDifference = ((this.investTotals.currentReturns - this.investTotals.totalInvestment) / this.investTotals.totalInvestment) * 100;
  };

  ngOnInit() {
  }

}

interface TotalsData {
  currentDate: Date;
  totalInvestment: number;
  currentReturns: number;
  percentDifference: number;
}
