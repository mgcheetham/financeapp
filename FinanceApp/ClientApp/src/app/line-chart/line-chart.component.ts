import { Component, Inject, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { getLocaleDateTimeFormat } from '@angular/common';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})

export class LineChartComponent {

  public investData: InvestmentData[];
  public http: HttpClient;
  public baseUrl: string;

  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];


  public lineChartData: ChartDataSets[] = [

    // Array of different segments in chart

    { data: [], label: 'Total Investment' },
    { data: [], label: 'Current Returns' }
  ];

  public lineChartLabels: Label[] = [];

  
  public lineChartOptions: ChartOptions = {

    // Define chart options
    responsive: true,
  };

  // Define colors of chart segments
  public lineChartColors: Color[] = [
    {
      // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
    },
    {
      // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
    }
  ];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.http = http;
    this.baseUrl = baseUrl;

    // Gets investment data for chart
    this.http.get<InvestmentData[]>(this.baseUrl + 'InvestmentData/GetChartData').subscribe(result => {

      this.investData = result;

      this.buildChartData();

    }, error => console.log(error));
  };

  private buildChartData() {

    // Loops through investment data for chart data
    for (var i of this.investData) {

      this.lineChartData[0].data.push(i.totalInvestment);
      this.lineChartData[1].data.push(i.currentReturns);

      var formatDate: string = new Date(i.currentDate).toLocaleDateString("UK");

      this.lineChartLabels.push(formatDate);
    }
  };

  // Events
  chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {

    //console.log(event, active);
  }

  chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {

    //console.log(event, active);
  }
}

interface InvestmentData {
  currentDate: Date;
  totalInvestment: number;
  currentReturns: number;
}
