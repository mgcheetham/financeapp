﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using FinanceApp.Controllers;
using FinanceApp.Data;
using Microsoft.EntityFrameworkCore;
using FinanceApp.Models;
using System.Threading.Tasks;
using System.Linq;
using FluentAssertions;

namespace FinanceAppTests
{
    [TestFixture]
    class InvestmentDataTests
    {
        private InvestmentDataController id;
        private DbContextOptions<FinanceAppContext> dbContextOptions;

        [SetUp]
        public void Setup()
        {
            // Uses the dbcontext options builder to create options to pass into context
            // In memory database creates a new database not attached to anything else
            dbContextOptions = new DbContextOptionsBuilder<FinanceAppContext>()
                       .UseInMemoryDatabase(databaseName: "InvestmentData")
                       .Options;

            // Create a new cryptocoins controller to test
            id = new InvestmentDataController(new FinanceAppContext(dbContextOptions));
        }

        [Test]
        public async Task GetChartDataTest()
        {
            // Creates a new db context to utilise
            using var context = new FinanceAppContext(dbContextOptions);

            // provides test data from model context
            SeedDB(context);

            var result = await id.GetChartData();
            var dataList = result.ToArray();

            // Checking the row count
            dataList.Count().Should().Be(2);

            // Checking the coinid value is not null
            dataList.All(x => x.currentDate == null).Should().BeFalse();
        }

        /// <summary>
        /// Seeds database context with new test data
        /// </summary>
        /// <param name="context"></param>
        private void SeedDB(FinanceAppContext context)
        {
            var investMentData = new[]
            {
                new InvestmentData {
                    ID = 1, currentDate = DateTime.Now, currentReturns = 200, totalInvestment = 20
                },
                new InvestmentData {
                    ID = 2, currentDate = DateTime.Now, currentReturns = 30, totalInvestment = 50
                },
            };

            context.InvestmentData.AddRange(investMentData);
            context.SaveChanges();
        }
    }
}
