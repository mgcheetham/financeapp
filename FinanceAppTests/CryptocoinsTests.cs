using NUnit.Framework;
using FinanceApp.Controllers;
using FinanceApp.Data;
using Microsoft.EntityFrameworkCore;
using FinanceApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using FluentAssertions;

namespace FinanceAppTests
{
    [TestFixture]
    public class CryptocoinsTests
    {
        private CryptocoinsController cc;
        private DbContextOptions<FinanceAppContext> dbContextOptions;

        [SetUp]
        public void Setup()
        {
            // Uses the dbcontext options builder to create options to pass into context
            // In memory database creates a new database not attached to anything else
            dbContextOptions = new DbContextOptionsBuilder<FinanceAppContext>()
                       .UseInMemoryDatabase(databaseName: "CryptocoinsData")
                       .Options;

            // Create a new cryptocoins controller to test
            cc = new CryptocoinsController(new FinanceAppContext(dbContextOptions));
        }

        [Test]
        public async Task GetCryptocoinsTest()
        {
            // Creates a new db context to utilise
            using var context = new FinanceAppContext(dbContextOptions);

            // provides test data from model context
            SeedDB(context);

            var result = await cc.GetCryptocoins();
            var dataList = result.ToArray();

            // Checking the row count
            dataList.Count().Should().Be(2);

            // Checking the coinid value is not null
            dataList.All(x => x.coinId == null).Should().BeFalse();
        }

        /// <summary>
        /// Seeds database context with new test data
        /// </summary>
        /// <param name="context"></param>
        private void SeedDB(FinanceAppContext context)
        {
            var coinsList = new[]
            {
                new Cryptocoins {
                    ID = 1,
                    datePurchased = DateTime.Now,
                    coinId = "bitcoin",
                    coinName = "bitcoin",
                    currentTotalValue = 50000,
                    currentValueCoin = 5000,
                    dateTotalValue = 400,
                    dateValueCoin = 3000,
                    purchaseAmount = 400
                },
                new Cryptocoins {
                    ID = 2,
                    datePurchased = DateTime.Now,
                    coinId = "ethereum",
                    coinName = "ethereum",
                    currentTotalValue = 50000,
                    currentValueCoin = 5000,
                    dateTotalValue = 400,
                    dateValueCoin = 3000,
                    purchaseAmount = 400
                },
            };

            context.Cryptocoins.AddRange(coinsList);
            context.SaveChanges();
        }
    }
}